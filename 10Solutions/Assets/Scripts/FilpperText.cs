﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FilpperText : MonoBehaviour
{
    [SerializeField]
    Text FliperLevelText;
    // Start is called before the first frame update
    void Start()
    {
      FliperLevelText = transform.GetChild(0).GetComponent<Text>();

        StartCoroutine(FliperTextLevel());
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator FliperTextLevel()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            FliperLevelText.gameObject.SetActive(true);
            yield return new WaitForSeconds(1.5f);
            FliperLevelText.gameObject.SetActive(false);
        }
        
    }
}
