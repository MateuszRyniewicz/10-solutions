﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelNameChange : MonoBehaviour
{
    [SerializeField]
    string LevelName;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = LevelName;   
    }

    
}
