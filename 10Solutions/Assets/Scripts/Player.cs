﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player instance;

    [SerializeField]
    Text _pointVis;
    [SerializeField]
    float _playerSpeed = 3f;
    [SerializeField]
    float _forceJump = 30f;
    [SerializeField]
    float _nextShotTime = 0.2f;
    [SerializeField]
    public int PlayerPoint;


    [SerializeField]
    GameObject _bullet;
    
    Rigidbody2D _rb;
    float _canShot = -1;

    public void Awake()
    {
        if (instance == null )
        {
            instance = this;
        }
        else if(instance!=this)
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
       
    }

    // Update is called once per frame
    void Update()
    {
        MovingPlayer();
        JumpingPlayer();
        ShothingPlayer();
        LoadGame();
    }
    public void MovingPlayer()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        transform.Translate(horizontal*_playerSpeed*Time.deltaTime, 0,0);       

    }
    public void JumpingPlayer()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            _rb.AddForce(Vector2.up * _forceJump * Time.deltaTime, ForceMode2D.Impulse);
        }
    }
    public void ShothingPlayer()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > _canShot)
        { 
            _canShot = Time.time + _nextShotTime;
        
            Instantiate(_bullet, transform.position + new Vector3(0.4f, 0f,0) ,Quaternion.identity);
        }
    }

    public void AddingPoint(int point)
    {
        PlayerPoint += point;
        ViewPoints(PlayerPoint);
        SavingPoints.instance.SavingGame();
    }
    public void ViewPoints( int viewPointVale)
    {
        _pointVis.text = viewPointVale.ToString();   
    }
    public void LoadGame()
    {
        PlayerPoint = SavingPoints.instance.LoadGame();
    }
}

