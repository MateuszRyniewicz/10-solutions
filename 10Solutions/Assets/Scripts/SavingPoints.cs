﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavingPoints : MonoBehaviour
{
   public static SavingPoints instance;


    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void SavingGame()
    {
        PlayerPrefs.SetInt("Points", Player.instance.PlayerPoint);
        PlayerPrefs.Save();
    }
    public int LoadGame()
    {
        if (PlayerPrefs.HasKey("Points"))
        {
            return PlayerPrefs.GetInt("Points");
        }
        else
        {
            return 0;
        }
    }
}
